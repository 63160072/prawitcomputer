/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.prawitcomputer;

/**
 *
 * @author sippu
 */
public class Mainboard {
    
    private String MainboardName;
    private int MainboardPrice;

    public Mainboard(String MainboardName, int MainboardPrice) {
        this.MainboardName = MainboardName;
        this.MainboardPrice = MainboardPrice;
    }

    public int getMainboardPrice() {
        return MainboardPrice;
    }
}
