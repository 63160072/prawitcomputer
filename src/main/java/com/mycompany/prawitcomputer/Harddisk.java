/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.prawitcomputer;

/**
 *
 * @author sippu
 */
public class Harddisk {

    private String HarddiskName;
    private int HarddiskGB;
    private int HarddiskPrice;
    

    public Harddisk(String HarddiskName, int HarddiskGB,int HarddiskPrice) {
        this.HarddiskName = HarddiskName;
        this.HarddiskGB = HarddiskGB;
        this.HarddiskPrice = HarddiskPrice;

    }

    public int getHarddiskPrice() {
        return HarddiskPrice;
    }
}
