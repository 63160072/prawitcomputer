/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.prawitcomputer;

/**
 *
 * @author sippu
 */
public class Memory {

    private String MemoryName;
    private int MemoryGB;
    private int MemoryPrice;


    public Memory(String MemoryName,int MemoryGB,int MemoryPrice) {
        this.MemoryName = MemoryName;
        this.MemoryGB = MemoryGB;
        this.MemoryPrice = MemoryPrice;
    }

    public int getMemoryPrice() {
        return MemoryPrice;
    }
}